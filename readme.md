### Install Docker
Install Docker for [macOS](https://docs.docker.com/docker-for-mac/install/).

Once installed, open a terminal:

To start the application
```sh
cd ~/gousto-tech-screen

docker-compose up --build recipes
```

To run the tests:
```sh
cd ~/gousto-tech-screen

docker-compose up --build recipes-test
```

The application is made up of two parts.

1. The CSVInteractor.

This is what is used to read from and write to the CSV data source. It reads in a very basic way, loading contents to memory

For writes, it attempts to use a very rudimentary locking system to prevent concurrent updates overwriting one another.

2. The Django views.

The views do the necessary transformation of data before it is sent to the CSV interactor or client.


##Functional Requirements:


### Use case 1 (curl copied directly from postman)
```sh
curl --request GET \
  --url http://127.0.0.1:8001/recipe/1/ \
  --header 'cache-control: no-cache' \
  --header 'postman-token: 460c2d17-79ed-5e4b-e26e-ae91684734bd'
```

Where 1 is the recipe ID


### Use case 2 (curl copied directly from postman)
```sh
curl --request GET \
  --url http://127.0.0.1:8001/recipe-by-cusine/asian/1/ \
  --header 'cache-control: no-cache' \
  --header 'postman-token: 6533da88-0cb0-a9a3-2541-30b561c6f6c8'
```

Where "asian" is the cuisine and 1 is the page number. The application should handle unexepcted page numbers (such as 0 or 100000) that don't match the number of recipes present in the data


### Use case 3 (curl copied directly from postman)
```
curl --request POST \
  --url http://127.0.0.1:8001/update_with_attributes/ \
  --header 'cache-control: no-cache' \
  --header 'content-type: application/json' \
  --header 'postman-token: d0ff1238-92ee-66ef-d66d-550be4271feb' \
  --data '{\n	"id":"1",\n	"box_type": "pescatarian"\n}'
```

Where the data object contains the identifying attribute (id) and any attributes to be edited. The application will handle a missing ID (by not updating) or superfluos attributes (by returning a 400)


## To Do list (In vague order)

- More non-happy path test coverage. Specifically around the entity update path
- Use typing / type annotations
- Don't obfuscate id in the attributes bundle for updating entities
- Would like to use a database but that wasn't allowed :D
- Worked around some Django CSRF settings that I probably should revisit, to make sure I've not messed something up
- Better use of response messages and response codes
