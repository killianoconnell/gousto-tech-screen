from recipes.csv_interactor import CSVInteractor
from django.test import TestCase
import os
from unittest.mock import patch, PropertyMock, MagicMock, ANY


class TestCSVInteractor(TestCase):
    CSVInteractor.data_source_name = os.path.join(os.path.dirname(__file__),
                                                  'test_recipe_data.csv')


    def test_fieldnames(self):
        csv_interactor = CSVInteractor()
        assert csv_interactor.fieldnames == [
            "id", "created_at", "updated_at", "box_type", "title", "slug",
            "short_title",
            "marketing_description", "calories_kcal", "protein_grams",
            "fat_grams",
            "carbs_grams", "bulletpoint1", "bulletpoint2", "bulletpoint3",
            "recipe_diet_type_id", "season", "base", "protein_source",
            "preparation_time_minutes", "shelf_life_days", "equipment_needed",
            "origin_country", "recipe_cuisine", "in_your_box",
            "gousto_reference"
        ]


    def test_keyed_by_id(self):
        csv_interactor = CSVInteractor()
        recipes = csv_interactor.read_keyed_by_id()
        assert recipes == {
            '1': {
                'id': '1',
                'created_at': '30/06/2015 17:58:00',
                'updated_at': '30/06/2015 17:58:00',
                'box_type': 'vegetarian',
                'title': 'Sweet Chilli and Lime Beef on a Crunchy Fresh Noodle Salad',
                'slug': 'sweet-chilli-and-lime-beef-on-a-crunchy-fresh-noodle-salad',
                'short_title': '',
                'marketing_description': "Here we've used onglet steak which is an extra flavoursome cut of beef that should never be cooked past medium rare. So if you're a fan of well done steak, this one may not be for you. However, if you love rare steak and fancy trying a new cut, please be",
                'calories_kcal': '401',
                'protein_grams': '12',
                'fat_grams': '35',
                'carbs_grams': '0',
                'bulletpoint1': '',
                'bulletpoint2': '',
                'bulletpoint3': '',
                'recipe_diet_type_id': 'meat',
                'season': 'all',
                'base': 'noodles',
                'protein_source': 'beef',
                'preparation_time_minutes': '35',
                'shelf_life_days': '4',
                'equipment_needed': 'Appetite',
                'origin_country': ANY,
                'recipe_cuisine': 'asian',
                'in_your_box': '',
                'gousto_reference': '59'
            },
            '2': {
                'id': '2',
                'created_at': '30/06/2015 17:58:00',
                'updated_at': '30/06/2015 17:58:00',
                'box_type': 'gourmet',
                'title': 'Tamil Nadu Prawn Masala',
                'slug': 'tamil-nadu-prawn-masala',
                'short_title': '',
                'marketing_description': "Tamil Nadu is a state on the eastern coast of the southern tip of India. Curry from there is particularly famous and it's easy to see why. This one is brimming with exciting contrasting tastes from ingredients like chilli powder, coriander and fennel seed",
                'calories_kcal': '524',
                'protein_grams': '12',
                'fat_grams': '22',
                'carbs_grams': '0',
                'bulletpoint1': 'Vibrant & Fresh',
                'bulletpoint2': 'Warming, not spicy',
                'bulletpoint3': 'Curry From Scratch',
                'recipe_diet_type_id': 'fish',
                'season': 'all',
                'base': 'pasta',
                'protein_source': 'seafood',
                'preparation_time_minutes': '40',
                'shelf_life_days': '4',
                'equipment_needed': 'Appetite',
                'origin_country': ANY,
                'recipe_cuisine': 'italian',
                'in_your_box': 'king prawns, basmati rice, onion, tomatoes, garlic, ginger, ground tumeric, red chilli powder, ground cumin, fresh coriander, curry leaves, fennel seeds',
                'gousto_reference': '58'
            }
        }


    def test_read_keyed_by_cuisine_all_fields(self):
        csv_interactor = CSVInteractor()
        recipes = csv_interactor.read_keyed_by_cuisine()

        assert recipes == {
            "asian": [{
                'id': '1',
                'created_at': '30/06/2015 17:58:00',
                'updated_at': '30/06/2015 17:58:00',
                'box_type': 'vegetarian',
                'title': 'Sweet Chilli and Lime Beef on a Crunchy Fresh Noodle Salad',
                'slug': 'sweet-chilli-and-lime-beef-on-a-crunchy-fresh-noodle-salad',
                'short_title': '',
                'marketing_description': "Here we've used onglet steak which is an extra flavoursome cut of beef that should never be cooked past medium rare. So if you're a fan of well done steak, this one may not be for you. However, if you love rare steak and fancy trying a new cut, please be",
                'calories_kcal': '401',
                'protein_grams': '12',
                'fat_grams': '35',
                'carbs_grams': '0',
                'bulletpoint1': '',
                'bulletpoint2': '',
                'bulletpoint3': '',
                'recipe_diet_type_id': 'meat',
                'season': 'all',
                'base': 'noodles',
                'protein_source': 'beef',
                'preparation_time_minutes': '35',
                'shelf_life_days': '4',
                'equipment_needed': 'Appetite',
                'origin_country': ANY,
                'recipe_cuisine': 'asian',
                'in_your_box': '',
                'gousto_reference': '59'
            }],
            "italian": [{
                'id': '2',
                'created_at': '30/06/2015 17:58:00',
                'updated_at': '30/06/2015 17:58:00',
                'box_type': 'gourmet',
                'title': 'Tamil Nadu Prawn Masala',
                'slug': 'tamil-nadu-prawn-masala',
                'short_title': '',
                'marketing_description': "Tamil Nadu is a state on the eastern coast of the southern tip of India. Curry from there is particularly famous and it's easy to see why. This one is brimming with exciting contrasting tastes from ingredients like chilli powder, coriander and fennel seed",
                'calories_kcal': '524',
                'protein_grams': '12',
                'fat_grams': '22',
                'carbs_grams': '0',
                'bulletpoint1': 'Vibrant & Fresh',
                'bulletpoint2': 'Warming, not spicy',
                'bulletpoint3': 'Curry From Scratch',
                'recipe_diet_type_id': 'fish',
                'season': 'all',
                'base': 'pasta',
                'protein_source': 'seafood',
                'preparation_time_minutes': '40',
                'shelf_life_days': '4',
                'equipment_needed': 'Appetite',
                'origin_country': ANY,
                'recipe_cuisine': 'italian',
                'in_your_box': 'king prawns, basmati rice, onion, tomatoes, garlic, ginger, ground tumeric, red chilli powder, ground cumin, fresh coriander, curry leaves, fennel seeds',
                'gousto_reference': '58'
            }]
        }


    def test_read_keyed_by_cuisine_some_fields(self):
        csv_interactor = CSVInteractor()
        recipes = csv_interactor.read_keyed_by_cuisine(["title", "gousto_reference"])

        assert recipes == {
            "asian": [{
                'title': 'Sweet Chilli and Lime Beef on a Crunchy Fresh Noodle Salad',
                'gousto_reference': '59'
            }],
            "italian": [{
                'title': 'Tamil Nadu Prawn Masala',
                'gousto_reference': '58'
            }]
        }


    def test_update_with_attributes_locked(self):
        CSVInteractor.update_locked = True
        csv_interactor = CSVInteractor()
        updated_recipe = csv_interactor.update_with_attributes({})
        assert updated_recipe is False
        CSVInteractor.update_locked = False


    def test_update_with_attributes_success(self):
        CSVInteractor.update_locked = False
        csv_interactor = CSVInteractor()
        updated_recipe = csv_interactor.update_with_attributes(
            {"id": 1, "title": "hope this works"}
        )
        assert updated_recipe.get('title') == "hope this works"

        recipes = csv_interactor.read_keyed_by_id()
        assert recipes.get("1").get("title") == "hope this works"
        csv_interactor.update_with_attributes(
            {"id": 1, "title": "Sweet Chilli and Lime Beef on a Crunchy Fresh Noodle Salad"}
        )
