from recipes.csv_interactor import CSVInteractor
from django.http import JsonResponse, HttpResponseBadRequest
from django.views.decorators.http import require_http_methods
import logging
import json
csv_interactor = CSVInteractor()

logger = logging.getLogger(__name__)


def test_recipe(request):
    """Returns a dummy recipe to check the health of the application

    Args:
        request (Request): Django request object

    Returns:
        Response
    """
    return JsonResponse({
        "id": 1,
        "box_type": "vegetarian",
        "title": "Beans on Toast",
        "slug": "beans-on-toast"
    })


def all_recipes(request):
    """Returns all recipes to check the health of the application interacting
    with the data store

    Args:
        request (Request): Django request object

    Returns:
        Response
    """
    all_recipes = csv_interactor.read_keyed_by_id()
    return JsonResponse(all_recipes)


def recipe_by_id(request, recipe_id):
    """Returns a single recipe, identified by the passed recipe_id

    Args:
        request (Request): Django request object
        recipe_id (int): id of the recipe being retrieved

    Returns:
        Response
    """
    all_recipes = csv_interactor.read_keyed_by_id()
    return JsonResponse(all_recipes.get(str(recipe_id), {}))


def recipe_by_cuisine(request, recipe_cuisine, page=1):
    """Returns all recipes for a supplied cuisine, if more than 10 exist. The
    result will be paged.

    Args:
        request (Request): Django request object
        recipe_cuisine (str): string of the cusine desired to filter recipes with
        page (int): the integer page number

    Returns:
        Response
    """
    if page <= 0:
        return HttpResponseBadRequest("Invalid page number")
    recipes_per_page = 10
    upper_bound = recipes_per_page * page
    lower_bound = upper_bound - recipes_per_page

    all_recipes_by_cuisine = csv_interactor.read_keyed_by_cuisine(fields=['id','title','marketing_description'])
    recipes_for_cuisine = all_recipes_by_cuisine.get(recipe_cuisine)
    total_number_of_recipes = len(recipes_for_cuisine)
    has_next = bool(recipes_for_cuisine[upper_bound:])
    has_prev = bool(recipes_for_cuisine[:lower_bound])
    recipes = recipes_for_cuisine[lower_bound:upper_bound]

    return JsonResponse(
        {
            "total_recipes": total_number_of_recipes,
            "has_next": has_next,
            "has_prev": has_prev,
            "recipes": recipes
        }
    )

@require_http_methods(['POST'])
def update_with_attributes(request):
    """Updates a recipe identified by an "id" in the postload, with the remaining
    attributes in the postload

    Args:
        request (Request): Django request object

    Returns:
        Response
    """
    recipe = json.loads(request.body)
    if not set(recipe.keys()).issubset(set(csv_interactor.fieldnames)):
        return HttpResponseBadRequest("Invalid recipe keys")

    updated_entity = csv_interactor.update_with_attributes(recipe)

    return JsonResponse(updated_entity)
