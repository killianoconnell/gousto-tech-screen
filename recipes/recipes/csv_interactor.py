import csv
import os
from tempfile import NamedTemporaryFile
import shutil
import logging


class CSVInteractor(object):

    logger = logging.getLogger(__name__)
    data_source_name = os.path.join(os.path.dirname(__file__), 'recipe-data.csv')
    update_locked = False

    def __init__(self):
        with open(self.data_source_name, 'r') as csvfile:
            reader = csv.DictReader(csvfile, skipinitialspace=True)
            self.fieldnames = reader.fieldnames


    def read_keyed_by_id(self):
        """Returns all recipes, keyed by their id

        Returns:
            dict of recipes
        """
        with open(self.data_source_name, mode='r') as file:
            recipes = {
                row['id']: {k: v for k, v in row.items()}
                for row in csv.DictReader(file, skipinitialspace=True)
            }

        return recipes


    def read_keyed_by_cuisine(self, fields=None):
        """Returns all recipes, grouped by their cuisine. Fields can be used to
        assert what fields are desired to be returned, if the whole object is not
        required

        Arguemnts:
          fields (list): list of fields to be included in the returned structures

        Returns:
            dict of recipes
        """
        fields = fields or self.fieldnames
        with open(self.data_source_name, mode='r') as file:
            recipes = {}
            for row in csv.DictReader(file, skipinitialspace=True):
                recipes.setdefault(row['recipe_cuisine'], []).append({k: v for k, v in row.items() if k in fields})

        return recipes


    def update_with_attributes(self, attributes):
        """Updates a recipe with an attributes bundle. id in attributes is used
        to identify the recipe. The remaining attributes are used to update the
        entity.

        Arguments:
          attributes (dict): mapping of values to idenitfy and update the entity.

        Returns:
            updated enitty
        """
        updated_entity = None
        if self.update_locked:
            return False
        try:
            self.update_locked = True
            tempfile = NamedTemporaryFile(mode='w', delete=False)
            with open(self.data_source_name, 'r') as csvfile, tempfile:
                reader = csv.DictReader(csvfile, skipinitialspace=True)
                writer = csv.DictWriter(tempfile, fieldnames=reader.fieldnames)
                writer.writeheader()
                for row in reader:
                    if row['id'] == str(attributes.get('id')):
                        for k, v in attributes.items():
                            row[k] = v
                        updated_entity = row

                    writer.writerow(row)
            shutil.move(tempfile.name, self.data_source_name)
        except Exception as e:
            self.logger(e)
            raise e
        finally:
            self.update_locked = False
            return updated_entity




