"""recipes URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from recipes.views import test_recipe, all_recipes, recipe_by_id, \
    recipe_by_cuisine, update_with_attributes


urlpatterns = [
    path('admin/', admin.site.urls),
    path('test-recipe/', test_recipe, name='test_recipe'),
    path('all-recipes/', all_recipes, name='all_recipes'),
    path('recipe/<int:recipe_id>/', recipe_by_id, name='recipe_by_id'),
    path('recipe-by-cusine/<str:recipe_cuisine>/<int:page>/', recipe_by_cuisine, name='recipe_by_cuisine'),
    path('update_with_attributes/', update_with_attributes, name='update_with_attributes')
]

