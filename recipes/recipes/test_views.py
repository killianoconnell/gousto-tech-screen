from django.urls import reverse
from django.test import TestCase
from unittest.mock import patch, PropertyMock, MagicMock


class TestRecipe(TestCase):

    def test_correct_response(self):
        response = self.client.get(reverse('test_recipe'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json(),
            {
                'id': 1,
                'box_type': 'vegetarian',
                'title': 'Beans on Toast',
                'slug': 'beans-on-toast'
            })


class AllRecipes(TestCase):

    @patch(
        'recipes.views.csv_interactor.read_keyed_by_id',
        return_value={
            "1": {
                'id': 1,
                'box_type': 'vegetarian',
                'title': 'Beans on Toast',
                'slug': 'beans-on-toast'
            },
            "2": {
                'id': 2,
                'box_type': 'pescatarian',
                'title': 'Tuna on Toast',
                'slug': 'tuna-on-toast'
            }
        }
    )
    def test_correct_response_returned_recipes_found(self, csv_mock):
        response = self.client.get(reverse('all_recipes'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json(),
            {
                "1": {
                    'id': 1,
                    'box_type': 'vegetarian',
                    'title': 'Beans on Toast',
                    'slug': 'beans-on-toast'
                },
                "2": {
                    'id': 2,
                    'box_type': 'pescatarian',
                    'title': 'Tuna on Toast',
                    'slug': 'tuna-on-toast'
                }
            })

    @patch(
        'recipes.views.csv_interactor.read_keyed_by_id',
        return_value={}
    )
    def test_correct_response_return_no_recipes(self, csv_mock):
        response = self.client.get(reverse('all_recipes'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {})


class RecipeById(TestCase):

    @patch(
        'recipes.views.csv_interactor.read_keyed_by_id',
        return_value={
            "1": {
                'id': 1,
                'box_type': 'vegetarian',
                'title': 'Beans on Toast',
                'slug': 'beans-on-toast'
            },
            "2": {
                'id': 2,
                'box_type': 'pescatarian',
                'title': 'Tuna on Toast',
                'slug': 'tuna-on-toast'
            }
        }
    )
    def test_correct_response_returned_recipes_found(self, csv_mock):
        response = self.client.get(reverse('recipe_by_id', args=[1]))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json(),
            {
                'id': 1,
                'box_type': 'vegetarian',
                'title': 'Beans on Toast',
                'slug': 'beans-on-toast',
            }
        )

    @patch(
        'recipes.views.csv_interactor.read_keyed_by_id',
        return_value={}
    )
    def test_correct_response_returned_no_recipes_found(self, csv_mock):
        response = self.client.get(reverse('recipe_by_id', args=[1]))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {})



class RecipeByCuisine(TestCase):

    @patch(
        'recipes.views.csv_interactor.read_keyed_by_cuisine',
        return_value={
            "college_student": [
                {
                    'id': 1,
                    'title': 'Beans on Toast',
                    'marketing_description': "A college staple!"
                },
                {
                    'id': 2,
                    'title': 'Tuna on Toast',
                    'marketing_description': 'A pescatarian college staple!'
                }
            ]
        }
    )
    def test_correct_response_returned_small_number_recipes_found(self, csv_mock):
        response = self.client.get(reverse('recipe_by_cuisine', args=["college_student", 1]))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json(),
            {
                "total_recipes": 2,
                "has_next": False,
                "has_prev": False,
                "recipes": [
                    {
                        'id': 1,
                        'title': 'Beans on Toast',
                        'marketing_description': "A college staple!"
                    },
                    {
                        'id': 2,
                        'title': 'Tuna on Toast',
                        'marketing_description': 'A pescatarian college staple!'
                    }
                ]
            }
        )


    @patch(
        'recipes.views.csv_interactor.read_keyed_by_cuisine',
        return_value={
            "college_student": [
                {
                    'id': 1,
                    'title': 'Beans on Toast',
                    'marketing_description': "A college staple!"
                },
                {
                    'id': 2,
                    'title': 'Tuna on Toast',
                    'marketing_description': 'A pescatarian college staple!'
                }
            ]
        }
    )
    def test_correct_response_page_out_of_bounds_for_number_of_recipes(self, csv_mock):
        response = self.client.get(reverse('recipe_by_cuisine', args=["college_student", 2]))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json(),
            {
                "total_recipes": 2,
                "has_next": False,
                "has_prev": True,
                "recipes": []
            }
        )

    @patch(
        'recipes.views.csv_interactor.read_keyed_by_cuisine',
        return_value={
            "college_student": [
                {
                    'id': x,
                    'title': 'Beans on Toast',
                    'marketing_description': "A college staple!"
                } for x in range(1, 23)
            ]
        }
    )
    def test_correct_response_more_recipes_than_page(self, csv_mock):
        response = self.client.get(reverse('recipe_by_cuisine', args=["college_student", 2]))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json(),
            {
                "total_recipes": 22,
                "has_next": True,
                "has_prev": True,
                "recipes": [
                    {
                        'id': x,
                        'title': 'Beans on Toast',
                        'marketing_description': "A college staple!"
                    } for x in range(11, 21)
                ]
            }
        )

    def test_correct_response_for_invalid_page(self):
        response = self.client.get(reverse('recipe_by_cuisine', args=["college_student", 0]))
        self.assertEqual(response.status_code, 400)


class UpdateWithAttributes(TestCase):

    @patch(
        'recipes.views.csv_interactor.fieldnames', return_value=['id']
    )
    def test_calling_update_attributes_not_in_field_names(self, fieldnames_mock):
        response = self.client.post(reverse('update_with_attributes'), {
            'id': 1,
            'title': 'Toast under beans'
        }, content_type='application/json')
        self.assertEqual(response.status_code, 400)

