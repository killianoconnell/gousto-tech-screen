FROM circleci/python:3.6.1

COPY requirements*.txt /opt/gousto/

COPY . /opt/gousto

WORKDIR /opt/gousto

RUN sudo pip install --upgrade pip
ARG PIP_REQUIREMENTS=requirements.txt
RUN pip install --no-cache-dir --no-warn-script-location -r /opt/gousto/$PIP_REQUIREMENTS --user

# tell the port number the container should expose
EXPOSE 8001

# run the application
ENTRYPOINT ["python3"]
CMD /opt/gousto/recipes/manage.py runserver 0.0.0.0:8001
